# K128
## Description

K128 is a multiplatform library written in Kotlin that allows parsing and working with GS1-128 codes (also known as GS1 DataBar codes).

## Installation

This library is built using Kotlin Multiplatform, enabling you to use it across various platforms such as:

- JavaScript
- iOS
- Android
- JVM

### Gradle (Android, Jvm)
To use K128 in your project, add the following dependency to your `build.gradle.kts` file:

```kotlin
dependencies {
    implementation("com.somaxa8:k128:0.1.0")
}
```

### NPM (Javascript)
```bash
npm install k128
```

### IOS

```kotlin
Soon
```

# Basic Usage

## Kotlin
### Parsing GS1-128 Codes

```kotlin
import com.somaxa8.k128.K128

fun main() {
    val gs128Code = "0123456789012345670123456789"
    val parsedData = K128.parse(gs128Code)

    for (data in parsedData) {
        println("AI: ${data.applicationIdentifier}, Name: ${data.name}, Data: ${data.data}")
    }
}
```

### Parsing GS1-128 Codes

```kotlin
import com.somaxa8.k128.K128

fun main() {
    val gs128Code = "012345678901234567-0123456789"
    val separator = '-'
    val parsedData = K128.parse(gs128Code, separator)

    for (data in parsedData) {
        println("AI: ${data.applicationIdentifier}, Name: ${data.name}, Data: ${data.data}")
    }
}
```

## Contributions 
If you wish to contribute to K128, feel free to open an issue or send a merge request on the repository.

## License
K128 is distributed under the Apache 2.0 License. See the LICENSE file for more information.