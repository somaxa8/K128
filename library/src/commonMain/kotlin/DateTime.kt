object DateTime {

    fun parseDate(dateString: String): String {
        if (dateString.length != 6) {
            throw IllegalArgumentException("Invalid date format. Expected YYMMDD.")
        }

        val year = "20" + dateString.substring(0, 2)
        val month = dateString.substring(2, 4)
        val day = dateString.substring(4, 6)

        return "$year-$month-$day"
    }

}
