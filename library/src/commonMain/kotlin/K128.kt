
object K128 {

    fun parse(code: String, groupSeparator: Char = '\u001D'): List<GS128Data> {
        val result = mutableListOf<GS128Data>()
        var index = 0

        while (index < code.length) {
            // Parse the Application Identifier (AI)
            val aiLength = determineAILength(code, index)

            val startIndex = index
            val endIndex = index + aiLength
            val ai = code.substring(startIndex, endIndex)
            index += aiLength

            // Get AI information
            val aiInfo = determineAIInfo(ai)

            // Parse the data
            val dataEndIndex = if (aiInfo.dataLength > 0) {
                (index + aiInfo.dataLength).coerceAtMost(code.length)
            } else {
                code.indexOf(groupSeparator, index).takeIf { it != -1 } ?: code.length
            }

            // Parse time if there is
            val data = code.substring(index, dataEndIndex)
            val formattedData = if (aiInfo.name.contains("date", ignoreCase = true)) {
                DateTime.parseDate(data)
            } else {
                data
            }

            result.add(GS128Data(ai, formattedData, aiInfo.name))

            // Move index to the end of the current data segment
            index = dataEndIndex

            // Skip the separator if present
            if (index < code.length && code[index] == groupSeparator) {
                index++
            }
        }

        return result
    }

    private fun determineAILength(code: String, startIndex: Int): Int {
        val twoCharAISet = setOf("00", "01", "02", "10", "11", "12", "13", "15", "16", "17", "20", "21", "22", "30", "37")
        val threeCharAISet = setOf("240", "241", "250", "251", "400", "401", "402", "403", "410", "411", "412", "413", "414", "415", "416", "420", "421", "422", "423", "424", "425", "426")
        val fourCharAISet = setOf("7001", "7002", "7003", "8001", "8002", "8003", "8004", "8005", "8006", "8007", "8008", "8018", "8019", "8020", "8110", "8200")

        return when {
            startIndex + 2 <= code.length && code.substring(startIndex, startIndex + 2) in twoCharAISet -> 2
            startIndex + 3 <= code.length && code.substring(startIndex, startIndex + 3) in threeCharAISet -> 3
            startIndex + 4 <= code.length && code.substring(startIndex, startIndex + 4) in fourCharAISet -> 4
            else -> 2
        }
    }

    private fun determineAIInfo(ai: String): AIInfo {
        val aiInfoMap = mapOf(
            "00" to AIInfo("SSCC", 18),
            "01" to AIInfo("GTIN", 14),
            "02" to AIInfo("GTIN of contained trade items", 14),
            "10" to AIInfo("Batch or lot number", -1),
            "11" to AIInfo("Production date", 6),
            "12" to AIInfo("Due date", 6),
            "13" to AIInfo("Packaging date", 6),
            "15" to AIInfo("Best before date", 6),
            "16" to AIInfo("Sell by date", 6),
            "17" to AIInfo("Expiration date", 6),
            "20" to AIInfo("Internal product variant", 2),
            "21" to AIInfo("Serial number", -1),
            "22" to AIInfo("Consumer unit / variant", -1),
            "240" to AIInfo("Additional product identification assigned by the manufacturer", -1),
            "241" to AIInfo("Customer part number", -1),
            "250" to AIInfo("Secondary serial number", -1),
            "251" to AIInfo("Reference to source entity", -1),
            "30" to AIInfo("Count of items (variable measure trade item)", -1),
            "37" to AIInfo("Count of trade items", -1),
            "400" to AIInfo("Customer purchase order number", -1),
            "401" to AIInfo("Global Identification Number for Consignment", -1),
            "402" to AIInfo("Global Shipment Identification Number (GSIN)", 17),
            "403" to AIInfo("Routing code", -1),
            "410" to AIInfo("Ship to - Deliver to Global Location Number", 13),
            "411" to AIInfo("Bill to - Invoice to Global Location Number", 13),
            "412" to AIInfo("Purchase from Global Location Number", 13),
            "413" to AIInfo("Ship for - Deliver for - Forward to Global Location Number", 13),
            "414" to AIInfo("Identification of a physical location", 13),
            "415" to AIInfo("Global Location Number of the invoicing party", 13),
            "416" to AIInfo("Global Location Number of the production or service location", 13),
            "420" to AIInfo("Ship to - Deliver to postal code within a single postal authority", -1),
            "421" to AIInfo("Ship to - Deliver to postal code with ISO country code", -1),
            "422" to AIInfo("Country of origin of a trade item", 3),
            "423" to AIInfo("Country of initial processing", -1),
            "424" to AIInfo("Country of processing", 3),
            "425" to AIInfo("Country of disassembly", 3),
            "426" to AIInfo("Country of full process", 3),
            "7001" to AIInfo("National Healthcare Reimbursement Number (NHRN) - Germany", 13),
            "7002" to AIInfo("Item Count", -1),
            "7003" to AIInfo("Expiration date and time", -1),
            "8001" to AIInfo("Roll products (width, diameter, core diameter, direction)", 14),
            "8002" to AIInfo("Cellular mobile telephone identifier", -1),
            "8003" to AIInfo("Global Returnable Asset Identifier (GRAI)", -1),
            "8004" to AIInfo("Global Individual Asset Identifier (GIAI)", -1),
            "8005" to AIInfo("Price per unit of measure", 6),
            "8006" to AIInfo("Identification of the trade items contained in a logistic unit", -1),
            "8007" to AIInfo("IBAN", -1),
            "8008" to AIInfo("Date and time of production", 12),
            "8018" to AIInfo("Global Service Relation Number (GSRN) - recipient", 18),
            "8019" to AIInfo("Global Service Relation Number (GSRN) - provider", 18),
            "8020" to AIInfo("Payment slip reference number", -1),
            "8110" to AIInfo("Coupon code identification for use in North America", -1),
            "8200" to AIInfo("Extended Packaging URL", -1)
        )

        return aiInfoMap[ai] ?: AIInfo("Unknown AI", -1)
    }
}
