data class GS128Data(
    val applicationIdentifier: String,
    val data: String,
    val name: String,
)
