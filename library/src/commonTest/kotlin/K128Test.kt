import kotlin.test.Test
import kotlin.test.assertEquals

class K128Test {

    @Test
    fun verify_GTIN_from_code() {
        val gs128Code = "0101234567890123-15101231"
        val parsedData = K128.parse(gs128Code, '-')

        val ai = "AI: ${parsedData[0].applicationIdentifier}, name: ${parsedData[0].name}, Data: ${parsedData[0].data}"
        println(ai)
        assertEquals(ai, "AI: 01, name: GTIN, Data: 01234567890123")
    }

    @Test
    fun verify_best_before_date_from_code() {
        val groupSeparator = '\u001D'
        val gs128Code = "0101234567890123${groupSeparator}15101231"
        val parsedData = K128.parse(gs128Code)

        val ai = "AI: ${parsedData[1].applicationIdentifier}, name: ${parsedData[1].name}, Data: ${parsedData[1].data}"
        println(ai)
        assertEquals(ai, "AI: 15, name: Best before date, Data: 2010-12-31")
    }
}